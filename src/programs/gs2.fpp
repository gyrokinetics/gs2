!> Main program. Used when running GS2 standalone, as opposed as a library for, e.g., Trinity.
!! Essentially this initializes a gs2_program_state_type 
!! object, and then calls the standard sequence of subroutines
!! from gs2_main to run the program. See gs2_main for more 
!! information.
!!
!! @note Currently FORD seems to ignore everything beyond the first line
!!
!! @note The following should probably be moved to a standalone file
!!
!! GS2 is an initial value nonlinear code which solves the gyrokinetic equation. This is the source code documentation for GS2, and is aimed at developers. 
!!
!! User Documentation
!! For a general introduction and user documentation please go to the Gyrokinetics Wiki: http://gyrokinetics.sourceforge.net/wiki/index.php/Main_Page
!! 
!! Some useful pages are: 
!!  - A beginners guide to downloading and installing: http://gyrokinetics.sourceforge.net/wiki/index.php/GS2:_A_Guide_for_Beginners
!!  - A general introduction to GS2: http://gyrokinetics.sourceforge.net/wiki/index.php/An_Introduction_to_GS2
!!  - A comprehensive guide to the GS2 input parameters: http://gyrokinetics.sourceforge.net/wiki/index.php/GS2_Input_Parameters
!!
!! @section doc Documentation Structure
!! Documentation is categorized by namespaces (i.e. modules), class (which in
!! fortran means custom types), and files. Within each namespace the subroutines
!! are documented in various ways, including developer comments and (very useful)
!! a chart showing which other subroutines  call the subroutine and which are
!! called by it.
!!
!! @section starting Starting Out
!!
!! If you want to start at the beginning and work out what GS2 does, start at
!! gs2.f90, and use the call graphs to follow the algorithm.
!!
!! @section Updating this documentation.
!! @section Updating Source Code Comments
!! This documentation is generated from commments added to the source code; to
!! add to it, add more comments to the <i>trunk</i> source. All documenting
!! comments begin with <tt>!></tt> and are continued with <tt>!!</tt>. For more help see the
!! doxygen help: http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html
!!
!! : gen Updating this Documentation
!!
!! - Install doxygen: http://www.stack.nl/~dimitri/doxygen/
!! - Go to the trunk folder and type 
!! 
!!  <tt> make doc sync_doc USER=[sourceforge user name]</tt>
program gs2
  use mp, only: init_mp, mp_comm, finish_mp
  use gs2_main, only : run_gs2, gs2_program_state_type, parse_command_line
  use gs2_optimisation, only: run_optimise_gs2
  implicit none
  type(gs2_program_state_type) :: state
  call parse_command_line()
  call init_mp
  state%mp_comm = mp_comm
  if (.not. run_optimise_gs2(state)) stop
  call run_gs2(state)
  call finish_mp
end program gs2

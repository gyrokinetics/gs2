README
======

This is a very basic test to check that all diagnostics in both the
old and new modules can run successfully. We don't check the values,
we're just checking that they don't crash

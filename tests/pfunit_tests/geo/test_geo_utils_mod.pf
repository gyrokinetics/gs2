module test_geo_utils_mod
  use geo_utils, only: abstract_geo_type
  use PFUNIT_MOD
  implicit none

  real, parameter :: derivative_tolerance = 5.e-3
  real, parameter :: value_tolerance = 5.e-12

  type, extends(abstract_geo_type) :: base_geo_test_type
   contains
     procedure :: read_and_set => read_and_set_null, finalise => null_noargs
     procedure :: finish_setup => null_noargs, rpos => null_rtheta
     procedure :: zpos => null_rtheta, psi => null_rtheta, rfun => null_rtheta
     procedure :: rcenter => null_rp, diameter => null_rp
     procedure :: btori => null_pbar, dbtori => null_pbar, qfun => null_pbar
     procedure :: pfun => null_pbar, dpfun => null_pbar, betafun => null_pbar
     procedure :: eqitem => null_eqitem
  end type base_geo_test_type

contains
  subroutine read_and_set_null(self, inputs)
    use geo_utils, only: geo_input_type
    class(base_geo_test_type), intent(in out) :: self
    type(geo_input_type), intent(in) :: inputs
  end subroutine read_and_set_null

  subroutine null_noargs(self)
    class(base_geo_test_type), intent(in out) :: self
  end subroutine null_noargs

  real function null_pbar(self, pbar)
    class(base_geo_test_type), intent(in) :: self
    real, intent(in) :: pbar
  end function null_pbar

  real function null_rp(self, rp)
    class(base_geo_test_type), intent(in) :: self
    real, intent(in) :: rp
  end function null_rp

  real function null_rtheta(self, r, theta)
    class(base_geo_test_type), intent(in) :: self
    real, intent(in) :: r, theta
  end function null_rtheta

  real function null_eqitem(self, r_in, theta_in, f, char) result(fstar)
    class(base_geo_test_type), intent(in) :: self
    real, intent(in) :: r_in, theta_in
    real, dimension(:, :), intent(in) :: f
    character(len=1), intent(in) :: char
  end function null_eqitem

  @test
  subroutine test_mod2pi()
    use constants, only : pi, twopi
    use geo_utils, only : mod2pi
    @assertEqual(0.0, mod2pi(0.0), tolerance=value_tolerance)
    @assertEqual(1.0, mod2pi(1.0), tolerance=value_tolerance)
    @assertEqual(-1.0, mod2pi(-1.0), tolerance=value_tolerance)
    @assertEqual(pi, mod2pi(pi), tolerance=value_tolerance)
    @assertEqual(-pi, mod2pi(-pi), tolerance=value_tolerance)
    @assertEqual(0.0, mod2pi(twopi), tolerance=value_tolerance)
    @assertEqual(0.0, mod2pi(-twopi), tolerance=value_tolerance)
    @assertEqual(pi / 2., mod2pi(2. * pi + (pi/2.)), tolerance=value_tolerance)
    @assertEqual(-pi / 2., mod2pi(pi + (pi/2.)), tolerance=value_tolerance)
  end subroutine test_mod2pi

  @test
  subroutine test_derm_even_function()
    use constants, only : pi
    integer, parameter :: nradius = 3
    integer, parameter :: ntheta = 128
    integer, parameter :: total_ntheta = 2*ntheta + 1
    integer, parameter :: total_size(2) = [nradius, total_ntheta]
    real, parameter :: theta_grid_spacing = 2 * pi / (total_ntheta - 1)
    real, dimension(nradius, -ntheta:ntheta) :: f_input
    real, dimension(nradius, -ntheta:ntheta, 2) :: df_result
    real, dimension(nradius, -ntheta:ntheta, 2) :: df_expected
    integer :: i, j
    type(base_geo_test_type) :: base_test
    base_test%nr = nradius ; base_test%nt = ntheta*2+1
    base_test%has_full_theta_range = .true.
    f_input = reshape([([(i*i*cos(j * theta_grid_spacing), &
         i=0, nradius-1)], j=-ntheta, ntheta)], total_size)

    df_expected(:, :, 1) = reshape( &
         [([0., 2., 4.]*cos(j * theta_grid_spacing), &
         j=-ntheta, ntheta)], total_size)
    df_expected(:, :, 2) = reshape( &
         [([(-i*i*sin(j * theta_grid_spacing) * theta_grid_spacing, &
         i=0, nradius-1)], j=-ntheta, ntheta)], total_size)

    call base_test%derm(f_input, df_result, 'E')

    @assertEqual(df_expected(:,:,1), df_result(:,:,1), tolerance=derivative_tolerance, message="Radial derivative")
    @assertEqual(df_expected(:,:,2), df_result(:,:,2), tolerance=derivative_tolerance, message="Theta derivative")
  end subroutine test_derm_even_function

  @test
  subroutine test_derm_odd_function()
    use constants, only : pi
    integer, parameter :: nradius = 3
    integer, parameter :: ntheta = 128
    integer, parameter :: total_ntheta = 2*ntheta + 1
    integer, parameter :: total_size(2) = [nradius, total_ntheta]
    real, parameter :: theta_grid_spacing = 2 * pi / (total_ntheta - 1)
    real, dimension(nradius, -ntheta:ntheta) :: f_input
    real, dimension(nradius, -ntheta:ntheta, 2) :: df_result
    real, dimension(nradius, -ntheta:ntheta, 2) :: df_expected
    integer :: i, j
    type(base_geo_test_type) :: base_test
    base_test%nr = nradius ; base_test%nt = ntheta*2+1
    base_test%has_full_theta_range = .true.

    f_input = reshape([([(i*i * sin(j * theta_grid_spacing), &
         i=0, nradius-1)], j=-ntheta, ntheta)], total_size)

    df_expected(:, :, 1) = reshape( &
         [([0., 2., 4.] * sin(j * theta_grid_spacing), &
         j=-ntheta, ntheta)], total_size)
    df_expected(:, :, 2) = reshape( &
         [([(i*i*cos(j * theta_grid_spacing) * theta_grid_spacing, &
         i=0, nradius-1)], j=-ntheta, ntheta)], total_size)
    call base_test%derm(f_input, df_result, 'O')

    @assertEqual(df_expected(:,:,1), df_result(:,:,1), tolerance=derivative_tolerance, message="Radial derivative")
    @assertEqual(df_expected(:,:,2), df_result(:,:,2), tolerance=derivative_tolerance, message="Theta derivative")
  end subroutine test_derm_odd_function

  @test
  subroutine test_derm_theta()
    use constants, only : pi
    integer, parameter :: nradius = 3
    integer, parameter :: ntheta = 128
    integer, parameter :: total_ntheta = 2*ntheta + 1
    integer, parameter :: total_size(2) = [nradius, total_ntheta]
    real, parameter :: theta_grid_spacing = 2 * pi / (total_ntheta - 1)
    real, dimension(nradius, -ntheta:ntheta) :: f_input
    real, dimension(nradius, -ntheta:ntheta, 2) :: df_result
    real, dimension(nradius, -ntheta:ntheta, 2) :: df_expected
    integer :: i, j
    type(base_geo_test_type) :: base_test
    base_test%nr = nradius ; base_test%nt = ntheta*2+1
    base_test%has_full_theta_range = .true.

    f_input = reshape([([(j*theta_grid_spacing, i=1, nradius)], j=-ntheta, ntheta)], total_size)

    df_expected(:, :, 1) = 0.
    df_expected(:, :, 2) = theta_grid_spacing
    call base_test%derm(f_input, df_result, 'T')

    @assertEqual(df_expected(:,:,1), df_result(:,:,1), tolerance=derivative_tolerance, message="Radial derivative")
    @assertEqual(df_expected(:,:,2), df_result(:,:,2), tolerance=derivative_tolerance, message="Theta derivative")
  end subroutine test_derm_theta

  ! This basically converts derivatives of R, Z from polar coordinates
  ! to cylindrical coordinates. R, Z are for a circle with radius 1
  @test
  subroutine test_eqdcart()
    use constants, only : pi
    integer, parameter :: nradius = 3
    integer, parameter :: ntheta = 4
    integer, parameter :: total_ntheta = 2*ntheta + 1
    integer, parameter :: total_size(2) = [nradius, total_ntheta]
    real, parameter :: theta_grid_spacing = 2 * pi / (total_ntheta - 1)
    real, dimension(nradius, -ntheta:ntheta, 2) :: drm, dzm
    real, dimension(nradius, -ntheta:ntheta, 2) :: R_cylindrical, Z_cylindrical
    real, dimension(nradius, -ntheta:ntheta, 2) :: R_cylindrical_expected, Z_cylindrical_expected
    integer :: i, j
    type(base_geo_test_type) :: base_test
    base_test%nr = nradius ; base_test%nt = ntheta*2+1
    base_test%has_full_theta_range = .true.

    ! Index-space derivatives of R
    drm(:, :, 1) = reshape( &
         [([(cos(j * theta_grid_spacing), &
         i=1, nradius)], j=-ntheta, ntheta)], total_size)
    drm(:, :, 2) = reshape( &
         [([(-sin(j * theta_grid_spacing) * theta_grid_spacing, &
         i=1, nradius)], j=-ntheta, ntheta)], total_size)

    ! Index-space derivatives of Z
    dzm(:, :, 1) = reshape( &
         [([(sin(j * theta_grid_spacing), &
         i=1, nradius)], j=-ntheta, ntheta)], total_size)
    dzm(:, :, 2) = reshape( &
         [([(cos(j * theta_grid_spacing) * theta_grid_spacing, &
         i=1, nradius)], j=-ntheta, ntheta)], total_size)

    call base_test%eqdcart(drm, drm, dzm, R_cylindrical)
    call base_test%eqdcart(dzm, drm, dzm, Z_cylindrical)

    R_cylindrical_expected(:,:,1) = 1.
    R_cylindrical_expected(:,:,2) = 0.
    Z_cylindrical_expected(:,:,1) = 0.
    Z_cylindrical_expected(:,:,2) = 1.

    ! why is the output only divided by the denominator on the r-range 2,nr?
    @assertEqual(R_cylindrical_expected(2:,:,1), R_cylindrical(2:,:,1), tolerance=value_tolerance)
    @assertEqual(R_cylindrical_expected(2:,:,2), R_cylindrical(2:,:,2), tolerance=value_tolerance)
    @assertEqual(Z_cylindrical_expected(2:,:,1), Z_cylindrical(2:,:,1), tolerance=value_tolerance)
    @assertEqual(Z_cylindrical_expected(2:,:,2), Z_cylindrical(2:,:,2), tolerance=value_tolerance)

  end subroutine test_eqdcart

  ! This basically converts derivatives of R, Z from cylindrical
  ! coordinates to Bishop coordinates. R, Z are for a circle with
  ! radius 1
  @test
  subroutine test_eqdbish()
    integer, parameter :: nradius = 3
    integer, parameter :: ntheta = 4
    real, dimension(nradius, -ntheta:ntheta, 2) :: dpcart
    real, dimension(nradius, -ntheta:ntheta, 2) :: R_cylindrical, Z_cylindrical
    real, dimension(nradius, -ntheta:ntheta, 2) :: R_cylindrical_in, Z_cylindrical_in
    real, dimension(nradius, -ntheta:ntheta, 2) :: R_cylindrical_expected, Z_cylindrical_expected
    type(base_geo_test_type) :: base_test
    base_test%nr = nradius ; base_test%nt = ntheta*2+1
    base_test%has_full_theta_range = .true.

    dpcart(:,:,1) = 1.
    dpcart(:,:,2) = 0.

    R_cylindrical_in(:,:,1) = 1.
    R_cylindrical_in(:,:,2) = 0.
    Z_cylindrical_in(:,:,1) = 0.
    Z_cylindrical_in(:,:,2) = 1.

    R_cylindrical_expected(:,:,1) = 1.
    R_cylindrical_expected(:,:,2) = 0.
    Z_cylindrical_expected(:,:,1) = 0.
    Z_cylindrical_expected(:,:,2) = 1.

    base_test%dpcart = dpcart
    call base_test%eqdbish(R_cylindrical_in, dpcart, R_cylindrical)
    call base_test%eqdbish(Z_cylindrical_in, dpcart, Z_cylindrical)

    ! why is the output only divided by the denominator on the r-range 2,nr?
    @assertEqual(R_cylindrical_expected(2:,:,1), R_cylindrical(2:,:,1), tolerance=value_tolerance)
    @assertEqual(R_cylindrical_expected(2:,:,2), R_cylindrical(2:,:,2), tolerance=value_tolerance)
    @assertEqual(Z_cylindrical_expected(2:,:,1), Z_cylindrical(2:,:,1), tolerance=value_tolerance)
    @assertEqual(Z_cylindrical_expected(2:,:,2), Z_cylindrical(2:,:,2), tolerance=value_tolerance)
  end subroutine test_eqdbish

end module test_geo_utils_mod

fields_local benchmark
======================

This benchmarks the `fields_local` field solve by timing repeated calls to the
function `getfield_local`. The number of field solves is controlled by `nstep`,
with default 20000.

indexing benchmark
==================

This benchmarks the indexing routines for `le_lo` by repeatedly calling 
`it_idx`, `ik_idx`, `ig_idx`, `is_idx` and `idx`. The number of calls
is controlled by `nstep`, with default 100000.


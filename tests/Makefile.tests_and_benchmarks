$(TEST_DIR)/Makefile.test_utils: ;
include $(TEST_DIR)/Makefile.test_utils

ifdef NPROCS
	NTESTPROCS:=$(NPROCS)
else
	NTESTPROCS:=2
endif

$(COVERAGE_DIR):
	$(QUIETSYM)$(MKDIR) $@

# To save time you can set test deps yourself on the command line:
# otherwise it builds everything just to be sure, because recursive
# make can't resolve dependencies
# UPDATE: Tests link against libgs2 now, so you can't choose 
# which dependencies to build any more. EGH 04/03/2015
# UPDATE: we now build and test the gs2 exec as well. EGH 14/03/15
TEST_DEPS?=$(LIBDIR)/libgs2.a $(OBJDIR)/functional_tests.o $(OBJDIR)/benchmarks.o gs2 write_default_input_file
#We need to pass in some extra libraries in some cases so currently have
#to wrap calls to make to ensure these get passed in. Ideally LIBS would
#already contain this information
MAKETESTS:=$(MAKE) "LIBS=-L$(LIBDIR) -lgs2 $(LIBS)"

#Set the level of testing, currently loosely defined as
# 0 : Only most basic tests included
# 1 : Most simple tests run
# 2 : More expensive tests (say a few minutes on 8 cores)
# 3 : Very expensive tests (say more than 10 minutes on 8 cores)
#etc.
TEST_LEVEL ?= 1

ifndef NO_TEST_COLOURS
# We have to change escape type from \033[0; to [ as otherwise
# sed interprets the leading \0 as print matching pattern.
SWITCH_ESCAPE_TYPE = $(subst \033[0;,[,$(1))
_TEST_GREEN := $(call SWITCH_ESCAPE_TYPE,$(C_GRE))
_TEST_RED := $(call SWITCH_ESCAPE_TYPE,$(C_RED))
_TEST_BLUE := $(call SWITCH_ESCAPE_TYPE,$(C_BLU))
_TEST_END := $(call SWITCH_ESCAPE_TYPE,$(C_RESET))
COLOUR_COMMAND:= sed -e \
	"s/passed/$(_TEST_GREEN)passed$(_TEST_END)/" -e \
	"s/FAILED/$(_TEST_RED)FAILED$(_TEST_END)/"   -e \
	"s/skipped/$(_TEST_BLUE)skipped$(_TEST_END)/"
else
COLOUR_COMMAND:= cat
endif

define TEST_PRINTOUT
	@echo ""
	@echo ""
	@echo "=================================="
	@echo "           Test Results      "
	@echo "==================================" $(1)
	@echo
	@echo "=================================="
endef

define TEST_RESULTS

	@echo
	@echo "=======   $(1)"
	@echo
	@find $(TEST_DIR)/$(1) -name results_of_test.txt -exec $(COLOUR_COMMAND) {} \;
endef

define RUN_TESTS
	@find $(TEST_DIR)/$(1) -name results_of_test.txt -delete
	@$(MAKETESTS) -C $(TEST_DIR)/$(1) --no-print-directory TEST_TYPE=$(2)
endef


.PHONY: tests unit_tests linear_tests nonlinear_tests benchmarks
.PHONY: unit_tests_no_message linear_tests_no_message
.PHONY: nonlinear_tests_no_message benchmarks_no_message
.PHONY: clean_tests clean_unit_tests clean_linear_tests clean_nonlinear_tests clean_benchmarks
.PHONY: tests_coverage test_script make_results_dir

distclean: clean_tests clean_benchmarks

clean_unit_tests: TYPE=Unit
clean_unit_tests:
	$(QUIETSYM)$(MAKE) clean_test_type -C $(TEST_DIR)/$(call lowerCase,$(TYPE))_tests TEST_TYPE=$(TYPE) --no-print-directory

clean_linear_tests: TYPE=Linear
clean_linear_tests:
	$(QUIETSYM)$(MAKE) clean_test_type -C $(TEST_DIR)/$(call lowerCase,$(TYPE))_tests TEST_TYPE=$(TYPE) --no-print-directory

clean_nonlinear_tests: TYPE=Nonlinear
clean_nonlinear_tests:
	$(QUIETSYM)$(MAKE) clean_test_type -C $(TEST_DIR)/$(call lowerCase,$(TYPE))_tests TEST_TYPE=$(TYPE) --no-print-directory

clean_tests: clean_unit_tests clean_linear_tests clean_nonlinear_tests
	@echo -n ''

clean_benchmarks:
	$(QUIETSYM)$(MAKE) clean_test_type -C $(TEST_DIR)/benchmarks TEST_TYPE=Benchmarks --no-print-directory

unit_tests_no_message: | $(TEST_RESULT_DIR)
unit_tests_no_message: $(TEST_DEPS)
	$(call RUN_TESTS,unit_tests,unit)

unit_tests: unit_tests_no_message
	$(call TEST_PRINTOUT,$(call TEST_RESULTS,$@))

linear_tests_no_message: | $(TEST_RESULT_DIR)
linear_tests_no_message: $(TEST_DEPS)
	$(call RUN_TESTS,linear_tests,linear)

linear_tests: linear_tests_no_message
	$(call TEST_PRINTOUT,$(call TEST_RESULTS,$@))

nonlinear_tests_no_message: | $(TEST_RESULT_DIR)
nonlinear_tests_no_message: $(TEST_DEPS)
	$(call RUN_TESTS,nonlinear_tests,nonlinear)

nonlinear_tests: nonlinear_tests_no_message
	$(call TEST_PRINTOUT,$(call TEST_RESULTS,$@))

$(TEST_RESULT_DIR): 
	@$(MKDIR) $(TEST_RESULT_DIR)

make_results_dir: | $(TEST_RESULT_DIR)

build_unit_tests: TEST_TARGET_TYPE:=build_test_exe
build_unit_tests: unit_tests

build_linear_tests: TEST_TARGET_TYPE:=build_test_exe
build_linear_tests: linear_tests

build_nonlinear_tests: TEST_TARGET_TYPE:=build_test_exe
build_nonlinear_tests: nonlinear_tests

build_tests: TEST_TARGET_TYPE:=build_test_exe
build_tests: tests

tests: |$(TEST_RESULT_DIR)
tests: unit_tests_no_message linear_tests_no_message nonlinear_tests_no_message
	$(call TEST_PRINTOUT,$(call TEST_RESULTS,unit_tests)$(call TEST_RESULTS,linear_tests)$(call TEST_RESULTS,nonlinear_tests))

ifeq ($(USE_GCOV),on)
tests_coverage: | $(COVERAGE_DIR)
tests_coverage: tests
	gcov $(shell find $(SRCDIR) $(SUBDIRS) -type f -name "*.f??" ) -p -o $(OBJDIR)
	mv *.gcov $(COVERAGE_DIR)/
else
tests_coverage:
	@echo "Cannot run coverage tests as gcov is not invoked."
	@echo "To use gcov, set the environment variable USE_GCOV=on and ensure your compiler supports gcov."
endif

test_script: test_script.sh

test_script.sh: unit_tests_no_message linear_tests_no_message
	echo "" > test_script.sh
	find $(PWD)/$(TEST_DIR)/unit_tests -executable | grep '$(TEST_DIR)/unit_tests/.*/' | sed -e 's/^\(.\+\)$$/\1 \1.in \&\&/' | sed -e 's/^/$(TESTEXEC) /'  >> test_script.sh
	find $(PWD)/$(TEST_DIR)/linear_tests -executable | grep '$(TEST_DIR)/linear_tests/.*/' | sed -e 's/^\(.\+\)$$/\1 \1.in \&\&/' | sed -e 's/^/$(TESTEXEC) /'  >> test_script.sh
	find $(PWD)/$(TEST_DIR)/nonlinear_tests -executable | grep '$(TEST_DIR)/nonlinear_tests/.*/' | sed -e 's/^\(.\+\)$$/\1 \1.in \&\&/' | sed -e 's/^/$(TESTEXEC) /'  >> test_script.sh
	echo "echo \"Tests Successful\"" >> test_script.sh

benchmarks_no_message: $(OBJDIR)/unit_tests.o $(TEST_DEPS)
	$(call RUN_TESTS,benchmarks,benchmark)

benchmarks: benchmarks_no_message
	$(call TEST_PRINTOUT,$(call TEST_RESULTS,$@))

clean: clean_coverage

clean_coverage:
ifeq ($(USE_GCOV),on)
	$(call messageRm,"Gcov files")	
	-$(QUIETSYM)rm -f $(COVERAGE_DIR)/*.gcov $(OBJDIR)/*.gcda $(OBJDIR)/*.gcno
endif

############################################################
# Recipes for Python-based tests

test_venv := $(GK_HEAD_DIR)/tests/venv

ifeq ($(GS2_HAS_PYTHON),yes)
# Create a virtualenv and install the test dependencies there
create-test-virtualenv:
	$(call messageInfo,"Creating virtualenv and installing test dependencies")
	sh -c "python3 -m venv $(test_venv); \
		. $(test_venv)/bin/activate; \
		pip install -r requirements.txt; \
		deactivate; \
	"
	@echo "----------------------------------------"
	$(call messageInfo,Now activate the virtualenv with '. $(test_venv)/bin/activate')
	$(call messageInfo,You can use 'deactivate' to stop the virtualenv)
else
create-test-virtualenv:
	@echo "No Python available, cannot create virtualenv"
endif

clean-test-virtualenv:
	$(call messageRm,Test virtualenv)
	-$(QUIETSYM)rm -rf $(test_venv)

distclean: clean-test-virtualenv

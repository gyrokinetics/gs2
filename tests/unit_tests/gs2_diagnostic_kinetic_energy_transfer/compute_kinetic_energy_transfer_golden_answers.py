#! /usr/bin/env python3
"""
Script to compute nonlinear transfer function

Copyright 2020 Steve Biggs

This file is part of initial.

initial is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

initial is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with initial.  If not, see http://www.gnu.org/licenses/.

Modified by Tobias Schuett on 17 June 2024
"""

from netCDF4 import Dataset
import numpy as np
import sys
import argparse


def compute_transfer_along_theta(ds, it):
    T_v_theta = np.zeros_like(theta)
    T_v_theta_kxs = np.zeros((ntheta, nkx))
    T_v_theta_kxt = np.zeros((ntheta, nkx))
    T_v_theta_kys = np.zeros((ntheta, nky))

    for i_theta in range(ntheta):
        T_v, T_v_kxs, T_v_kxt, T_v_kys = compute_transfer_wrapper(ds, i_theta, it)
        T_v_theta[i_theta] = T_v
        T_v_theta_kxs[i_theta, :] = T_v_kxs
        T_v_theta_kxt[i_theta, :] = T_v_kxt
        T_v_theta_kys[i_theta, :] = T_v_kys

    return T_v_theta, T_v_theta_kxs, T_v_theta_kxt, T_v_theta_kys

def compute_transfer_wrapper(ds, i_theta, it):
    print(f"Computing net transfer for theta = {theta[i_theta]:.4f}")
    return compute_net_transfer_this_theta(
        ds["phi_t"][it, :, :, i_theta, :], ds["bmag"][i_theta]
    )


def compute_net_transfer_this_theta(phi, B):
    # Convert ri dimension to complex dtype
    phi = ri_to_complex(phi)
    # Make full versions of arrays to simplify indexing
    phi = make_full(phi)
    # Pre-shift arrays so that indexing works correctly
    phi = np.fft.fftshift(phi, axes=(0, 1))
    # Pre-compute complex conjugate array
    phi_star = np.conj(phi[ikyt, ...])
    # Pre-prepare array of mediators for performance
    phi_m = compute_phi_m(phi, ikx0, iky0)
    # calculate B_factor
    B_factor = 1 / B**3
    # Compute transfer and return
    return compute_net_transfer(
        phi, phi_star, phi_m, B_factor
    )


def ri_to_complex(array_ri):
    array_complex = np.zeros(array_ri.shape[:-1], dtype=complex)
    array_complex.real = array_ri[..., 0]
    array_complex.imag = array_ri[..., 1]
    return array_complex


def move_first_axis_to_last(array_orig):
    # Roll first axis to last place, e.g. for improved cache performance
    # NB: Cannot use np.moveaxis(...) as the return value is just a view of the original
    array_new = np.zeros(
        np.concatenate((array_orig.shape[1:], [array_orig.shape[0]])),
        dtype=array_orig.dtype,
    )
    for i in range(array_orig.shape[0]):
        array_new[..., i] = array_orig[i, ...]
    return array_new


def make_full(array):
    # Assumes dimension order is ky, kx{, t - single size}
    # For concatenated array:
    # - ky slice = -1:0:-1 so that we include non-zero kys in reverse order
    # - kx slice = -1::-1 so that we include all kxs in reverse order but this puts kx = 0
    #     to the end even though we still want it at the start, hence use of
    #     np.roll(..., 1, axis=1) so that kx = 0 is at ikx = 0
    # - np.conj(...) of the reversed and rolled array as the negative kys are the conjugate
    #     of the positive kys
    # - np.concatenate(..., axis=0) so that we concatenate in the ky direction
    return np.concatenate(
        (array, np.conj(np.roll(array[-1:0:-1, -1::-1], 1, axis=1))),
        axis=0,
    )


def compute_phi_m(phi, ikx0, iky0):
    # Pre-allocate array
    # NB: Cannot pre-allocate this variable in the global scope and re-use it here
    phi_m = np.zeros((nky, nkx, nkx), dtype=complex)
    # Loop over target and source wavenumbers
    for ikys in range(nky):
        for ikxs in range(nkx):
            for ikxt in range(nkx):
                # Work out index of mediator
                ikxm = ikxt - ikxs + ikx0
                ikym = ikyt - ikys + iky0
                # Check mediator index exists
                if not (0 <= ikxm and ikxm < nkx and 0 <= ikym and ikym < nky):
                    # Just don't set a value to avoid unnecessary cache misses
                    continue
                # Store mediator value in mediator array
                phi_m[ikys, ikxs, ikxt] = phi[ikym, ikxm]
    # Return output array
    return phi_m


def compute_net_transfer(phi, phi_star, phi_m, B_factor):
    T_v_kys_kxs_kxt = (
                            2 * B_factor
                            * z_hat_dot_k_cross_k_prime
                            * k_factor_for_T_v
                            * compute_time_average(
                                (
                                    np.reshape(phi_star, (1, 1, nkx))
                                    * phi_m
                                    * np.reshape(phi, (nky, nkx, 1))
                                ).real
                            )
                        )

    T_v = np.sum(T_v_kys_kxs_kxt)
    T_v_kxs = np.sum(T_v_kys_kxs_kxt, axis=(0,2)) # shape (kys, kxs, kxt) -> (kxs)
    T_v_kxt = np.sum(T_v_kys_kxs_kxt, axis=(0,1)) # shape (kys, kxs, kxt) -> (kxt)
    T_v_kys = np.sum(T_v_kys_kxs_kxt, axis=(1,2)) # shape (kys, kxs, kxt) -> (kys)

    return T_v, T_v_kxs, T_v_kxt, T_v_kys


def compute_time_average(field):
    "Computes the time average of an N-D field assuming time is the last axis"
    time_average = field
    return time_average

def write_output():
    np.savetxt("kinetic_energy_transfer_theta_golden_answer.txt", T_v)
    np.savetxt("kinetic_energy_transfer_theta_kxsource_golden_answer.txt", T_v_kxs)
    np.savetxt("kinetic_energy_transfer_theta_kxtarget_golden_answer.txt", T_v_kxt)
    np.savetxt("kinetic_energy_transfer_theta_kysource_golden_answer.txt", T_v_kys)

if __name__ == "__main__":
    # input
    parser = argparse.ArgumentParser(description="Check kinetic energy transfer diagnostic against golden answers.")

    parser.add_argument('--file', help='Path to GS2 output .nc file (with .out.nc extension).',
                    required = True)

    parser.add_argument('--it', help='Time index to analyse', default = -2, type = int)
    parser.add_argument('--write', help='If set writes results to file', action = 'store_true')

    args = parser.parse_args()

    input_filename = args.file
    it = args.it

    # Open Dataset
    ds = Dataset(input_filename)

    ## NOTE: negative indexing -> positive indexing #######
    print("len(t): ", len(ds["t"]))
    print("it: ", it)
    #######################################################

    # Pre-compute various quantities for performance
    theta = ds["theta"]
    ntheta = len(ds["theta"])
    kx = np.fft.fftshift(ds["kx"])
    nkx = len(kx)
    ky = np.fft.fftshift(np.concatenate((ds["ky"], -ds["ky"][-1:0:-1])))
    nky = len(ky)
    ikx0 = np.argmin(np.abs(kx))
    iky0 = np.argmin(np.abs(ky))
    ikyt = iky0  # i.e. target is always zonal flows
    t = ds["t"][it]
    nt = 1
    z_hat_dot_k_cross_k_prime = np.reshape(
        np.reshape(kx, (1, nkx)) * np.reshape(ky, (nky, 1)),
        (nky, 1, nkx),
    )
    k_factor_for_T_v = -np.reshape(
        np.reshape(kx, (1, nkx)) * np.reshape(kx, (nkx, 1)),
        (1, nkx, nkx),
    )
    # Do calculation
    #result = parallel_compute_transfer_along_theta()
    T_v, T_v_kxs, T_v_kxt, T_v_kys = compute_transfer_along_theta(ds, it)
    # Write output
    if args.write:
        write_output()

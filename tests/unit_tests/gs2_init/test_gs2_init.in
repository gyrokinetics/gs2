!=================================================================
!                        GS2 INPUT FILE
!=================================================================
!
!  GS2 is a gyrokinetic flux tube initial value turbulence code
!  which can be used for fusion or astrophysical plasmas.
!
!  Website:
!            https://gyrokinetics.gitlab.io/gs2/
!  Repo:
!            https://bitbucket.org/gyrokinetics/gs2/
!  Citation:
!            https://zenodo.org/record/2551066
!  doi:
!            10.5281/zenodo.2551066
!
!=================================================================

!==============================
!GENERAL PARAMETERS  
!==============================
&kt_grids_knobs
 grid_option = "box" ! The general layout of the perpendicular grid.
/

!==============================
!  
!==============================
&kt_grids_box_parameters
 y0 = 2
 !naky = 2
 ny = 4
 jtwist = 6
 nx = 6
/

!==============================
!  
!==============================
&theta_grid_parameters
 ntheta = 12 ! Number of points along field line (theta) per 2 pi segment
 nperiod = 1 ! Number of 2 pi segments along equilibrium magnetic field.
 eps = 0.18 ! eps=r/R
 epsl = 2.0 ! epsl=2 a/R
 shat = 0.8 ! 
 pk = 1.44 ! pk = 2 a / q R
 shift = 0.0 ! shift = -R q**2 dbeta/drho (>0)
/

!==============================
!  
!==============================
&theta_grid_knobs
 equilibrium_option = "eik" ! Controls which geometric assumptions are used in the run.
/
&theta_grid_eik_knobs
 local_eq = .true.
/

!==============================
!  
!==============================
&theta_grid_salpha_knobs
 model_option = "default" 
/

!==============================
!PITCH ANGLE/ENERGY GRID SETUP  
!==============================
&le_grids_knobs
 ngauss = 3 ! Number of untrapped pitch-angles moving in one direction along field line.
 negrid = 12 ! Total number of energy grid points
/

!==============================
!BOUNDARY CONDITIONS  
!==============================
&dist_fn_knobs
 gridfac = 1.0 ! Affects boundary condition at end of theta grid.
 omprimfac = 1.0 
 boundary_option = "linked" ! Sets the boundary condition along the field line (i.e. the boundary conditions at theta = +- pi).
 adiabatic_option = "iphi00=2" ! The form of the adiabatic response (if a species is being modeled as adiabatic).
 g_exb = 0.0 
 nonad_zero = .true. ! If true switches on new parallel boundary condition where h=0 at incoming boundary instead of g=0.
/

!==============================
!ALGORITHMIC CHOICES  
!==============================
&fields_knobs
 field_option = "local" ! Controls which time-advance algorithm is used for the linear terms.
 force_maxwell_reinit = .true.
/

!==============================
!  
!==============================
&knobs
 beta = 0.1 ! Ratio of particle to magnetic pressure (reference Beta, not total beta):  beta=n_0 T_0 /( B^2 / (8 pi))
 zeff = 1.0 ! Effective ionic charge.
 wstar_units = .false. ! For linear runs only. Evolves each k_y with a different timestep.
 fphi = 1.0 ! Multiplies Phi (electrostatic potential).
 fapar = 1.0 ! Multiplies A_par. Use 1 for finite beta (electromagnetic), 0 otherwise (electrostatic)
 fbpar = 1.0
 delt = 0.05 ! Time step
 nstep = 4 ! Maximum number of timesteps
/

!==============================
!  
!==============================
&reinit_knobs
 delt_adj = 2.0 ! When the time step needs to be changed, it is adjusted 
 delt_minimum = 1.0e-06 ! The minimum time step is delt_minimum.
/

!==============================
!  
!==============================
&layouts_knobs
 layout = "lexys" ! 'yxles', 'lxyes', 'lyxes', 'lexys' Determines the way the grids are laid out in memory.
/

!==============================
!COLLISIONS  
!==============================
&collisions_knobs
 collision_model = "none" ! Collision model used in the simulation. Options: 'default', 'none', 'lorentz', 'ediffuse'
/

!==============================
!NONLINEARITY  
!==============================
&nonlinear_terms_knobs
 nonlinear_mode = "off" ! Include nonlinear terms? ('on','off')
 cfl = 0.5 ! The maximum delt < cfl * min(Delta_perp/v_perp)
/

!==============================
!EVOLVED SPECIES  
!==============================
&species_knobs
 nspec = 2 ! Number of kinetic species evolved.
/

!==============================
!SPECIES PARAMETERS 1 
!==============================
&species_parameters_1
 z = 1.0 ! Charge
 mass = 1.0 ! Mass
 dens = 1.0 ! Density
 temp = 1.0 ! Temperature
 tprim = 6.9 ! -1/T (dT/drho)
 fprim = 2.2 ! -1/n (dn/drho)
 uprim = 0.0 ! ?
 vnewk = 0.01 ! collisionality parameter
 type = "ion" ! Type of species, e.g. 'ion', 'electron', 'beam'
/



&species_parameters_2
 z = -1.0 ! Charge
 mass = 0.0002723311546840959 ! Mass
 dens = 1.0 ! Densit
 temp = 1.0 ! Temperature
 tprim = 6.9 ! -1/T (dT/drho)
 fprim = 2.2 ! -1/n (dn/drho)
 uprim = 0.0 ! ?
 vnewk = 0.01 ! collisionality parameter
 type = "electron" ! Type of species, e.g. 'ion', 'electron', 'beam'
/


!==============================
! 1 
!==============================
&dist_fn_species_knobs_1
 fexpr = 0.45 ! Temporal implicitness parameter. Recommended value: 0.48
 bakdif = 0.05 ! Spatial implicitness parameter. Recommended value: 0.05
/


&dist_fn_species_knobs_2
 fexpr = 0.45 ! Temporal implicitness parameter. Recommended value: 0.48
 bakdif = 0.05 ! Spatial implicitness parameter. Recommended value: 0.05
/

!==============================
!INITIAL CONDITIONS  
!==============================
&init_g_knobs
 constant_random_flag = .true.
 chop_side = .false. ! Rarely needed. Forces asymmetry into initial condition.
 phiinit = 0.001 ! Average amplitude of initial perturbation of each Fourier mode.
 ginit_option = "noise" ! Sets the way that the distribution function is initialized.
/

!==============================
!DIAGNOSTICS  
!==============================
&gs2_diagnostics_knobs
 print_flux_line = .F. ! Instantaneous fluxes output to screen
/

!==============================
!  
!==============================
&general_f0_parameters
 alpha_f0 = "analytic" ! Form of the alpha equilibrium distribution function: "maxwellian", "analytic" or "external"
 energy_0 = 0.01 ! Lower limit of F_alpha for : F_alpha(energy_0)=0.
/


!=================================================================
!                        GS2 INPUT FILE
!=================================================================
!
!  GS2 is a gyrokinetic flux tube initial value turbulence code
!  which can be used for fusion or astrophysical plasmas.
!
!  Website:
!            https://gyrokinetics.gitlab.io/gs2/
!  Repo:
!            https://bitbucket.org/gyrokinetics/gs2/
!  Citation:
!            https://zenodo.org/record/2551066
!  doi:
!            10.5281/zenodo.2551066
!
!=================================================================

!==============================
!GENERAL PARAMETERS  
!==============================
&kt_grids_knobs
 grid_option = "single" ! The general layout of the perpendicular grid.
/

!==============================
!  
!==============================
&kt_grids_single_parameters
 aky = 1.0 ! The actual value of ky rho
 akx = 0.0 
/

!==============================
!  
!==============================
&theta_grid_parameters
 ntheta = 20 ! Number of points along field line (theta) per 2 pi segment
 nperiod = 1 ! Number of 2 pi segments along equilibrium magnetic field.
 eps = 0.0 ! eps=r/R
 epsl = 0.0 ! epsl=2 a/R
 shat = 1.0e-06 ! 
 pk = 1.0 ! pk = 2 a / q R
 btor_slab = 0.0 ! Determines the angle between the field and the flow in the slab limit
/

!==============================
!  
!==============================
&theta_grid_knobs
 equilibrium_option = "s-alpha" ! Controls which geometric assumptions are used in the run.
/

!==============================
!  
!==============================
&theta_grid_salpha_knobs
 model_option = "const-curv" 
/

!==============================
!PITCH ANGLE/ENERGY GRID SETUP  
!==============================
&le_grids_knobs
 vcut = 2.5 ! No. of standard deviations from the standard Maxwellian beyond which the distribution function will be set to 0
 ngauss = 8 ! Number of untrapped pitch-angles moving in one direction along field line.
 negrid = 8 ! Total number of energy grid points
/

!==============================
!BOUNDARY CONDITIONS  
!==============================
&dist_fn_knobs
 boundary_option = "periodic" ! Sets the boundary condition along the field line (i.e. the boundary conditions at theta = +- pi).
 adiabatic_option = "iphi00=2" ! The form of the adiabatic response (if a species is being modeled as adiabatic).
/

!==============================
!ALGORITHMIC CHOICES  
!==============================
&fields_knobs
 field_option = "implicit" ! Controls which time-advance algorithm is used for the linear terms.
/

!==============================
!  
!==============================
&knobs
 beta = 0.0 ! Ratio of particle to magnetic pressure (reference Beta, not total beta):  beta=n_0 T_0 /( B^2 / (8 pi))
 wstar_units = .false. ! For linear runs only. Evolves each k_y with a different timestep.
 fphi = 1.0 ! Multiplies Phi (electrostatic potential).
 fapar = 0.0 ! Multiplies A_par. Use 1 for finite beta (electromagnetic), 0 otherwise (electrostatic)
 fbpar = 0.0 ! Multiplies B_parallel. Use 1 for high beta, 0 otherwise.
 delt = 0.05 ! Time step
 nstep = 5000 ! Maximum number of timesteps
 seed = 10000
/

!==============================
!  
!==============================
&layouts_knobs
 layout = "lexys" ! 'yxles', 'lxyes', 'lyxes', 'lexys' Determines the way the grids are laid out in memory.
 local_field_solve = .false. ! Strongly affects initialization time on some parallel computers.
/

!==============================
!COLLISIONS  
!==============================
&collisions_knobs
 collision_model = "none" ! Collision model used in the simulation. Options: 'default', 'none', 'lorentz', 'ediffuse'
 conserve_moments = .true. 
/

!==============================
!EVOLVED SPECIES  
!==============================
&species_knobs
 nspec = 1 ! Number of kinetic species evolved.
/

!==============================
!SPECIES PARAMETERS 1 
!==============================
&species_parameters_1
 z = 1.0 ! Charge
 mass = 1.0 ! Mass
 dens = 1.0 ! Density	
 temp = 1.0 ! Temperature
 tprim = 6.0 ! -1/T (dT/drho)
 fprim = 1.0 ! -1/n (dn/drho)
 uprim = 0.0 ! ?
 vnewk = 0.0 ! collisionality parameter
 type = "ion" ! Type of species, e.g. 'ion', 'electron', 'beam'
/

!==============================
! 1 
!==============================
&dist_fn_species_knobs_1
 fexpr = 0.49 ! Temporal implicitness parameter. Recommended value: 0.48
 bakdif = 0.01 ! Spatial implicitness parameter. Recommended value: 0.05
/

!==============================
!INITIAL CONDITIONS  
!==============================
&init_g_knobs
 chop_side = .false. ! Rarely needed. Forces asymmetry into initial condition.
 width0 = -4.0 ! Initial perturbation has Gaussian envelope in theta with width width0,
 phiinit = 0.1 ! Average amplitude of initial perturbation of each Fourier mode.
 ginit_option = "noise" ! Sets the way that the distribution function is initialized.
 zf_init = 0.0 
/

!==============================
!DIAGNOSTICS  
!==============================
&gs2_diagnostics_knobs
 print_flux_line = .false. ! Instantaneous fluxes output to screen
 write_fluxes = .false. ! Write nonlinear fluxes as a function of time.
 print_line = .false. ! Estimated frequencies and growth rates to the screen/stdout
 write_verr = .false. ! Write velocity space diagnostics to '.lpc' and '.verr' files
 write_g = .false. ! Write the distribution function to the '.dist' (NetCDF?)
 write_line = .false.. ! If (write_ascii = T) write estimated frequencies and growth rates to the output file
  ! write_gyx not specified --- Write dist fn at a given physical spacial point to a file
 write_heating = .false. ! Write heating rate, collisonal entropy generation etc to '.heat'
 write_final_epar = .false. ! If (write_ascii = T) E_parallel(theta) written to runname.eigenfunc
 write_avg_moments = .false. ! Write flux surface averaged low-order moments of g to runname.out.nc and runname.moments (if write_ascii = T)
 write_lorentzian = .false. ! Frequency Sweep Data
 write_omega = .false. ! If (write_ascii = T) instantaneous omega to output file. Very heavy output
 write_omavg = .true. ! If (write_ascii = T) time-averaged growth rate and frequency to the output file.
 write_eigenfunc = .false. ! If (write_ascii = T) Normalized phi written to runname.eigenfunc
 write_final_fields = .true. ! If (write_ascii = T) Phi(theta) written to '.fields'
 write_final_moments = .true. ! write final n, T
 nwrite = 10 ! Output diagnostic data every nwrite
 navg = 10 ! Any time averages performed over navg
 omegatol = -0.001 ! The convergence has to be better than one part in 1/omegatol
 omegatinst = 500.0 ! Recommended value: 500.
 save_for_restart = .false. ! Write restart files.
/


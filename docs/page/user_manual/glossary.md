---
title: Glossary
---

## Abbreviations and Acronyms

GS2 uses a variety of conventional abbreviations and acronyms through the code,
at least some of which are only used in GS2. These abbreviations may be used in
comments or as part of variable names.  This is a non-exhaustive list of those
abbreviations and their meanings.

- **adj**, **fac** or **mult**: generally a multiplicative factor for another
  quantity
- **ant**: "antenna"
- **avg**: "average"
- **bess**: "Bessel function"
- **bish**: Related to the Bishop formalism
- **conv**: "convergence"
- **cv**: The curvature drift
- **db**: \(\delta B\)
- **dens**: "density"
- **dfn** or **dist_fn**: "distribution function". This generally refers to `g`,
  the GS2 adjusted piece of the non-adiabatic distribution function.
- **eig**: "eigenmode" or "eigenvalue"
- **epar**: \(E_\Vert\)
- **eq**: "equilibrium"
- **exb**: related to the \(E \times B\) velocity or force
- **fun**: "function"
- **gb**: The \(\nabla B\) drift
- **inv**: "inverse"
- **le**: "lambda, energy", the pitch-angle and energy dimensions
- **lo**: relating to one of the layouts (FIXME: link to some layouts
  documentation)
- **ob**: "outboard", the low-field side of a tokamak
- **pol**: "poloidal"
- **pri**, **prim**, **del**, **diff**, **grad**: derivative
- **surf**: "surface"
- **sym**: "symmetry"
- **th**: "theta"
- **tor**: "toroidal"
- **trap**: "trapped"
- **ttp**: "totally trapped particle"
- **verb**: "verbosity"
- **vnew** or **vn**: related to collisionality or collision frequency
- **wfb**: "World's fattest banana", the pitch angle where the parallel velocity
  goes to zero at \(B_{max}\)

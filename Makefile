#
#
#  Makefile for GS2 Gyrokinetic Turbulence code 
#
#  (requires GNU's gmake)
#
GK_PROJECT := gs2
# Always try to look in Makefiles directory when including makefiles
MAKEFLAGS+=-IMakefiles
#
#  Makefile written by Bill Dorland and Ryusuke Numata
#  Modified by GS2 contributors
#
# * Requires a Fortran 2008 Standard compliant compiler
# 
# * Frequently Tested Hosts, Systems include recent Fedora, Ubuntu, Archer2.
#
# Should work on most modern Linux/Unix and Mac OS X. Not tested on Windows.
#
# * Switches:
#
# Here is a list of switches with simple explanation.
# In the brackets, values accepted are shown,
# where "undefined" means blank.
# Switches with (bin) are checked if they are defined or not
# What values they have do not matter.
# Be careful that DEBUG=off means DEBUG=on.
#
# Fortran Standard Specification (95, 2003, 2008, undefined)
FORTRAN_SPEC ?=
# turns on debug mode (bin)
DEBUG ?=
# turns on scalasca instrumentation mode (bin)
SCAL ?=
# turns on profile mode (gprof,ipm)
PROF ?=
# optimization (on,aggressive,undefined)
OPT ?= on
# prevents linking with shared libraries (bin)
STATIC ?=
# promotes precisions of real and complex (bin)
DBLE ?= on
# promotes precisions of double and double complex (bin)
QUAD ?=
# turns on distributed memory parallelization using MPI (mpi2, mpi3)
USE_MPI ?= mpi2
# turns on SHMEM parallel communications on SGI (bin)
USE_SHMEM ?=
# which FFT library to use (fftw,fftw3,mkl_fftw,undefined) 
USE_FFT ?= fftw3
# uses netcdf library (bin)
USE_NETCDF ?= on
# uses parallel netcdf library
USE_PARALLEL_NETCDF ?= 
# uses lapack or compatible library (bin)
USE_LAPACK ?=
# Use local random number generator (mt,undefined)
# see also README
USE_LOCAL_RAN ?=
# Use Fortran 2003, 2008 Intrinsics (bin)
# (Note this overwhelms other special function options)
USE_F200X_INTRINSICS ?=
# Use local special functions (bin)
USE_LOCAL_SPFUNC ?= 
# Use openmp (very limited use currently)
USE_OPENMP ?=
# Include higher-order terms in GK equation arising from low-flow physics
LOWFLOW ?=
# Compile with PETSC/SLEPC support for determining eigensystem (bin).
USE_EIG ?=
# Deprecated form
WITH_EIG ?=
USE_FIELDS_EIG ?=
# Include LAPACK and GSL libraries for genquad scheme
GENQUAD ?=
# Compile with gcov flags to check for code coverage
USE_GCOV ?=
# Name of python exe
GS2_PYTHON_NAME ?= python3
# Name of pytest executable
GS2_PYTEST_NAME ?= pytest
# A user-settable name for this particular build of GS2
GS2_BUILD_TAG ?= None
# Compile with the new simplified diagnostics module (bin)
USE_NEW_DIAG ?=on
# If on, build with position independent code, needed if GS2 is going to be included
# in a shared library or linked dynamically
# This *may* have performance implications, but it hasn't been tested yet.
USE_FPIC ?= 
# Defines the size of the constant character arrays that are used to store the run
# name. 2000 has been selected as the default as being longer than most people will 
# need. However, this can be overriden by setting this parameter either in the environment
# or when executing make. Make sure you rebuild constants.fpp if you want to change this.
# e.g. 
#       $ touch utils/constants.fpp
#       $ make RUN_NAME_SIZE=<newsize>
RUN_NAME_SIZE?=2000
# If defined then don't show the full command being run everywhere
QUIET           ?= yes
# If defined then use escape commands to colourise some output
# Note hack to support both spellings
# We default WITH_COLOR (and hence WITH_COLOUR) to on if
# MAKE_TERMOUT is set. This is an internal make variable which is set
# provided the output is going to a terminal.  In other words we don't
# default the colour on if we're redirecting output to file.
ifdef MAKE_TERMOUT
WITH_COLOR ?= yes
endif
WITH_COLOUR     ?= $(WITH_COLOR)
# If defined then show the full path in compilation statements etc.
SHOW_ABS_PATH ?= yes

###############################################################
# Experimental features
###############################################################
# Do we use the experimental memory usage reporting
GS2_EXPERIMENTAL_ENABLE_MEMORY_USAGE ?=

###############################################################
# By default we export all variables defined in this Makefile
# or any included makefiles. This gives sub-make invocations
# (i.e. make calls from within our Makefile) access to the same
# variables. Really we should just default what is needed.
export
###############################################################

#
# * Targets:
#
#  depend: generate dependency
#  test_make: print values of variables
#  clean: clean up
#  distclean: does "make clean" + removes platform links & executables
#
###############################################################
# A default empty rule to say we can't rebuild this makefile so don't bother trying
Makefile: ;
############################################################### DEFAULT VALUE
#
# These variables can be set in platform-dependent Makefile.
#
SHELL           = bash
MAKE		= make
CPP		= cpp -traditional
FPPFLAGS	=
FC		= f90
MPIFC		?= mpif90
F90FLAGS	=
F90OPTFLAGS	= 
LD 		= $(FC)
LDFLAGS 	= $(F90FLAGS)
ARCH 		= ar
ARCHFLAGS 	= cr
RANLIB		= ranlib
PERL		= perl
FORD           ?= $(GK_HEAD_DIR)/scripts/ford_interceptor.py
MKDIR           = mkdir -p
MPI_INC	?=
MPI_LIB ?=
FFT_INC ?=
FFT_LIB ?=
EIG_INC ?=
EIG_LIB ?=
GENQUAD_INC ?=
GENQUAD_LIB ?=
NETCDF_INC ?=
NETCDF_LIB ?=
LAPACK_INC ?=
LAPACK_LIB ?=
NAG_LIB ?=
NAG_PREC ?= dble
MACRO_DEFS ?=

# Makefile.local may override some options set in Makefile.$(GK_SYSTEM),
# thus it is included before and after Makefile.$(GK_SYSTEM)
Makefile.local: ;
sinclude Makefile.local

#### directories

#Record the top level path. Note we don't just use $(PWD) as this
#resolves to the directory from which make was invoked. The approach
#taken here ensures that GK_HEAD_DIR is the location of this Makefile.
#Note the realpath call removes the trailing slash so later we need
#to add a slash if we want to address subdirectories
GK_THIS_MAKEFILE := $(abspath $(lastword $(MAKEFILE_LIST)))
GK_HEAD_DIR := $(realpath $(dir $(GK_THIS_MAKEFILE)))

#Directories to store outputs of different types
OBJDIR:= $(GK_HEAD_DIR)/objs
MODDIR:= $(GK_HEAD_DIR)/include
BINDIR:= $(GK_HEAD_DIR)/bin
LIBDIR:= $(GK_HEAD_DIR)/lib
GENERATED_SRC_DIR:= $(GK_HEAD_DIR)/generated_src

#Src files exist here
SRCDIR:= $(GK_HEAD_DIR)/src

#Programs exist here
PROGDIR:= $(SRCDIR)/programs

#Extra subdirectories which include code that might be needed
SUBDIRS= geo diagnostics
SUBDIRS:= $(addprefix $(SRCDIR)/, $(SUBDIRS))

#External subdirectories which include code that might be needed
EXTERNALDIR:= $(GK_HEAD_DIR)/externals
EXTERNALSRCDIRS:= utils
EXTERNALSRCDIRS:= $(addprefix $(EXTERNALDIR)/, $(EXTERNALSRCDIRS))

SUBDIRS += $(EXTERNALSRCDIRS)

MAKEFILES_DIR:=$(GK_HEAD_DIR)/Makefiles
GEO:=$(SRCDIR)/geo
UTILS:=$(EXTERNALDIR)/utils
NEASYF := $(EXTERNALDIR)/neasyf/src

AUTO_GEN_CONFIG_DIR:= $(SRCDIR)/config_auto_gen

#Tests
TEST_DIR:=$(GK_HEAD_DIR)/tests
TEST_RESULT_DIR:= $(GK_HEAD_DIR)/test-results
COVERAGE_DIR:= $(GK_HEAD_DIR)/coverage

#Patch file
PATCHFILE:= .patch.make

######################################################### PLATFORM DEPENDENCE

# compile mode switches (DEBUG, PROF, OPT, STATIC, DBLE)
# must be set before loading Makefile.$(GK_SYSTEM) because they may affect
# compiler options.

# include system-dependent make variables
ifndef GK_SYSTEM
	ifdef SYSTEM
$(warning SYSTEM environment variable is obsolete)
$(warning use GK_SYSTEM instead)
	GK_SYSTEM = $(SYSTEM)
	else
$(error GK_SYSTEM is not set)
	endif
endif

include Makefile.$(GK_SYSTEM)

# Files that we know are going to be in the submodule directories.
# This is in case the directories exist but the submodules haven't
# actually been initialised
MAKEFILES_SENTINEL := $(MAKEFILES_DIR)/Makefile.bitbucket
UTILS_SENTINEL := $(UTILS)/mp.fpp
NEASYF_SENTINEL := $(NEASYF)/neasyf.f90

.PRECIOUS: $(MAKEFILES_SENTINEL) $(UTILS_SENTINEL) $(NEASYF_SENTINEL)

# Make sure the submodules exist; they all currently share the same recipe
$(NEASYF_SENTINEL) $(UTILS_SENTINEL) $(MAKEFILES_SENTINEL) submodules:
	@echo "Downloading submodules"
	git submodule update --init --recursive

.Makefile.makefile_exists_helper: ;

# Here we have a dependency on $(MAKEFILES_SENTINEL) such that this
# will fetch the submodules first. We then try to build a null target
# in a helper makefile which includes Makefile.$(GK_SYSTEM). This helper
# makefile has a recipe to built Makefile.$(GK_SYSTEM) which just raises
# an error -- this allows us to abort if the makefile can't be found even
# after fetching the submodules
Makefile.$(GK_SYSTEM): $(MAKEFILES_SENTINEL)
	$(QUIETSYM)$(MAKE) -f .Makefile.makefile_exists_helper --no-print-directory

# Here we add dependency on the utils_sentinel for a somewhat arbitrary
# file in utils that we will try to include. If $(UTILS)/define.inc is not
# present this dependency will ensure the submodules recipe is run.
$(UTILS)/define.inc: $(UTILS_SENTINEL)

# Here we try to include this file from utils. We don't want (or get) any
# information from including it, it is merely to trigger the submodules
# recipe when utils hasn't been checked out
include $(UTILS)/define.inc

# include Makefile.local if exists
sinclude Makefile.local

#Include utility makefile
Makefile.utils: ;
include Makefile.utils

#Indicate that we don't want to rebuild the COMPILER makefile
$(MAKEFILES_DIR)/Makefile.$(COMPILER): ;
#############################################################################

GIT_HASH:=$(shell git rev-parse HEAD)
MACRO_DEFS+=-DGIT_HASH='"$(GIT_HASH)"'

GIT_HASH_MAKEFILES:=$(shell git submodule status $(MAKEFILES_DIR) | cut -d ' ' -f2)
MACRO_DEFS+=-DGIT_HASH_MAKEFILES='"$(GIT_HASH_MAKEFILES)"'

GIT_HASH_UTILS:=$(shell git submodule status $(UTILS) | cut -d ' ' -f2)
MACRO_DEFS+=-DGIT_HASH_UTILS='"$(GIT_HASH_UTILS)"'

# Version string, including current commit if not on a release, plus
# '-dirty' if there are uncommitted changes
GIT_VERSION := $(shell git describe --tags --dirty --match "[0-9]*.[0-9]*.[0-9]*")
MACRO_DEFS += -DGIT_VERSION='"$(GIT_VERSION)"'

#The magic sed esacpes either ( or ) by replacing with \( or \)
GIT_BRANCH:=$(shell git branch | grep \* | cut -d ' ' -f2- | sed 's|\([()]\)|\\\1|g')
MACRO_DEFS+=-DGIT_BRANCH='"$(GIT_BRANCH)"'

# Find if there are any modified tracked files
ifeq ($(shell git status --short -uno -- . | wc -l), 0)
	GIT_STATE:="clean"
$(shell touch $(PATCHFILE))
else
	GIT_STATE:="modified"

# Here we make a patch file, which could be read by the program if we wanted to keep a record
$(shell git diff -w -- .  > $(PATCHFILE))
endif

MACRO_DEFS+=-DGIT_STATE='$(GIT_STATE)'
MACRO_DEFS+=-DGIT_PATCH_FILE='"$(PATCHFILE)"'

######## PYTHON ###############################################################

PYTHON_EXE:=$(shell which $(GS2_PYTHON_NAME) 2>/dev/null)
GS2_HAS_PYTHON:=$(if $(PYTHON_EXE),yes,no)
ifeq ($(GS2_HAS_PYTHON),yes)
PYTHON_VERSION:=$(shell $(PYTHON_EXE) --version 2>&1)
else
$(warning Could not find Python, looking for '$(GS2_PYTHON_NAME)'. \
You will not be able to generate some files, or run some tests. \
Please make sure '$(GS2_PYTHON_NAME)' is in your '$$PATH', or set \
"GS2_PYTHON_NAME" to the correct name of the Python executable on your system.\
Python will soon become a hard-dependency to build GS2.
)
endif

PYTEST := $(shell which $(GS2_PYTEST_NAME) 2>/dev/null)
GS2_HAS_PYTEST := $(if $(PYTEST),yes,no)

###############################################################################

# Define RUN_NAME_SIZE
MACRO_DEFS+=-DRUN_NAME_SIZE=$(RUN_NAME_SIZE) 

############################################################################# Deal with user flags

# Define IS_RELEASE based on if current commit exactly matches
# a tagged version. If it does then we define RELEASE to reflect
# this tag. If it doesn't then we define RELEASE from the "closest"
# tag plus a modifier.
IS_RELEASE := $(shell git describe --tags --exact-match 2>/dev/null)
MACRO_DEFS+=-DIS_RELEASE=$(IS_RELEASE)

ifneq ($(IS_RELEASE),) # If we're on a commit corresponding to a tag
	RELEASE:=$(IS_RELEASE)
else
	RELEASE:=$(shell git describe --tags 2>/dev/null)
endif
MACRO_DEFS+=-DRELEASE='"$(RELEASE)"'

# Define GK_SYSTEM for runtime_tests so that we can get the system name
# at runtime
MACRO_DEFS+=-DGK_SYSTEM='"$(GK_SYSTEM)"'

ifndef USE_NETCDF
ifdef USE_NEW_DIAG
$(error 'USE_NETCDF is off/undefined. The new diagnostics module requires netcdf. Please build without the new diagnostics module (i.e. set USE_NEW_DIAG=) ')
endif
endif

ifndef USE_FFT
$(warning USE_FFT is undefined/off)
$(warning Nonlinear runs are therefore not sensible)
endif

ifdef USE_MPI
	FC = $(MPIFC)
	MACRO_DEFS += -DMPI
endif

ifeq ($(USE_MPI),mpi3)
	MACRO_DEFS += -DMPI3
endif

ifeq ($(USE_NEW_DIAG),on)
	MACRO_DEFS += -DNEW_DIAG
endif

ifeq ($(USE_FPIC),on)
	F90FLAGS += -fPIC
endif

ifdef USE_SHMEM
	MACRO_DEFS += -DSHMEM
endif

ifeq ($(findstring fftw3,$(USE_FFT)),fftw3)
	MACRO_DEFS += -DFFT=_FFTW3_
	FFT_LIB ?= -lfftw3
endif

ifdef GENQUAD # Should connect to USE_LAPACK?
	GENQUAD_LIB ?= -llapack 
	MACRO_DEFS += -DGENQUAD
endif

ifdef USE_NETCDF
	NETCDF_LIB ?= -lnetcdf
	MACRO_DEFS += -DNETCDF
endif

# Default to compressed netcdf files. Note we choose to default to no
# compression for builds with parallel netcdf due to an issue reported
# on Macs and JFRS-1 in which compression and parallel netcdf leads to
# hanging code. Hopefully future investigation can give a solution
# which allows both features to be used together and we can remove
# this complication.
ifdef USE_PARALLEL_NETCDF
	USE_NETCDF_COMPRESSION ?=
else
	USE_NETCDF_COMPRESSION ?= on
endif

ifdef USE_NETCDF_COMPRESSION
	MACRO_DEFS += -DGK_NETCDF_DEFAULT_COMPRESSION_ON
endif

ifdef USE_PARALLEL_NETCDF
	MACRO_DEFS += -DNETCDF_PARALLEL
endif

ifeq ($(USE_LOCAL_RAN),mt)
	MACRO_DEFS += -DRANDOM=_RANMT_
endif

ifdef USE_LAPACK
	MACRO_DEFS += -DLAPACK
endif

ifdef USE_OPENMP
	MACRO_DEFS += -DOPENMP
ifeq ($(USE_FFT),fftw3)
ifdef DBLE
	FFT_LIB += -lfftw3_omp
else
	FFT_LIB += -lfftw3f_omp
endif
endif
endif

ifdef USE_F200X_INTRINSICS
	MACRO_DEFS += -DF200X_INTRINSICS -DSPFUNC=_SPF200X_
else
	ifdef USE_LOCAL_SPFUNC
		MACRO_DEFS += -DSPFUNC=_SPLOCAL_
	endif
endif

ifndef DBLE
	MACRO_DEFS += -DSINGLE_PRECISION
endif

ifdef QUAD
	DBLE = on
	MACRO_DEFS += -DQUAD_PRECISION
endif

ifdef LOWFLOW
	MACRO_DEFS += -DLOWFLOW
$(warning $(shell echo -e "$(C_RED)")\
LOWFLOW is deprecated in 8.2 and will be removed in the future\
$(shell echo -e "$(C_END)"))
endif

#Support old deprecated option
ifdef WITH_EIG
$(warning WITH_EIG is deprecated, set USE_EIG instead. Forcing USE_EIG=on.)
	USE_EIG := on
endif

#Setup the flags for using the eigensolver
ifdef USE_EIG
ifdef USE_FIELDS_EIG
	MACRO_DEFS += -DFIELDS_EIG
endif
	EIG_INC += -I$(PETSC_DIR)/include -I$(SLEPC_DIR)/include
	ifdef PETSC_ARCH
		EIG_LIB += -L$(PETSC_DIR)/$(PETSC_ARCH)/lib
		EIG_INC += -I$(PETSC_DIR)/$(PETSC_ARCH)/include
		EIG_LIB += -L$(SLEPC_DIR)/$(PETSC_ARCH)/lib
		EIG_INC += -I$(SLEPC_DIR)/$(PETSC_ARCH)/include
	else
		EIG_LIB += -L$(PETSC_DIR)/lib
	endif
#SLEPC_ARCH is non-standard and is unlikely to be set, it should
#really be PETSC_ARCH (as used above)
	ifdef SLEPC_ARCH
		EIG_LIB += -L$(SLEPC_DIR)/$(SLEPC_ARCH)/lib
		EIG_INC += -I$(SLEPC_DIR)/$(SLEPC_ARCH)/include
	else
		EIG_LIB += -L$(SLEPC_DIR)/lib
	endif
	EIG_LIB += -lslepc -lpetsc

	MACRO_DEFS += -DWITH_EIG
endif 

ifdef SCAL
   FC:= scalasca -instrument $(FC)
endif

# NOTE: we use a _shell variable_ here to expand GS2_BUILD_TAG to
# ensure that it gets quoted correctly on the command line
MACRO_DEFS += -DGS2_BUILD_TAG='"${GS2_BUILD_TAG}"'

############################################################################# Done dealing with user flags

ALL_SOURCE_FILES := $(wildcard $(addsuffix /*.fpp, $(SRCDIR) $(PROGDIR) $(SUBDIRS)))
ALL_SOURCE_FILES += $(wildcard $(addsuffix /*.f90, $(SRCDIR) $(PROGDIR) $(SUBDIRS)))
ALL_SOURCE_FILES += $(wildcard $(addsuffix /*.F90, $(SRCDIR) $(PROGDIR) $(SUBDIRS)))


# most common include and library directories
DEFAULT_INC_LIST := $(UTILS) $(GEO) $(NEASYF) $(AUTO_GEN_CONFIG_DIR) $(SRCDIR)
DEFAULT_LIB_LIST :=
# This default library path list can simplify the procedure of porting,
# however, I found this (actually -L/usr/lib flag) causes an error
# when linking gs2 at bassi (RS6000 with xl fortran)
DEFAULT_INC:=$(foreach tmpinc,$(DEFAULT_INC_LIST),$(shell [ -d $(tmpinc) ] && echo -I$(tmpinc)))
DEFAULT_LIB:=$(foreach tmplib,$(DEFAULT_LIB_LIST),$(shell [ -d $(tmplib) ] && echo -L$(tmplib)))

LIBS	+= $(DEFAULT_LIB) $(MPI_LIB) $(NETCDF_LIB) $(FFT_LIB) \
		   $(NAG_LIB) $(EIG_LIB) $(GENQUAD_LIB) $(LAPACK_LIB)
INCLUDES = $(DEFAULT_INC) $(MPI_INC) $(NETCDF_INC) $(FFT_INC)\
		   $(EIG_INC) $(GENQUAD_INC) $(PFUNIT_INC) $(LAPACK_INC)
INCLUDES += -I$(MODDIR)

# Here we attempt to remove non-existent directories from INCLUDES. We assume that
# INCLUDES is currently just a list of entries of the form -I<some directory> so for
# each entry we strip the -I and then echo -I<some directory> if <some directory> is
# found to exist. This can help avoid warnings about non-existent directories
INCLUDES := $(foreach tmpvp,$(INCLUDES),$(shell [ -d $(echo $(tmpvp) | sed 's/^ *-I//g') ] && echo $(tmpvp) ))

F90FLAGS += $(F90OPTFLAGS) $(INCLUDES)
F90FLAGS += $(FC_PREPROCESS_FLAG) $(FC_FREE_FORM_FLAG)
FPPFLAGS += $(MACRO_DEFS)

VPATH = $(SUBDIRS) $(MODDIR) $(SRCDIR) $(PROGDIR) $(GENERATED_SRC_DIR) $(NEASYF)
# this just removes non-existing directory from VPATH
VPATH_tmp := $(foreach tmpvp,$(subst :, ,$(VPATH)),$(shell [ -d $(tmpvp) ] && echo $(tmpvp)))
VPATH := .:$(shell echo $(VPATH_tmp) | sed "s/ /:/g")

# Commands for generating repetitive Fortran files from python scripts
ifeq ($(GS2_HAS_PYTHON),yes)

$(SRCDIR)/gs2_init_down.inc $(SRCDIR)/gs2_init_level_array.inc $(SRCDIR)/gs2_init_subroutines.inc $(SRCDIR)/gs2_init_level_list.inc: $(SRCDIR)/generate_gs2_init.py
	$(call messageOp,"Generating from python","\\t:",$(call processName,$@) ($(call processName,$^)))
	$(QUIETSYM)$(PYTHON_EXE) $< $@

$(GENERATED_SRC_DIR)/gs2_init.f90: $(SRCDIR)/gs2_init.fpp $(SRCDIR)/gs2_init_level_array.inc $(SRCDIR)/gs2_init_subroutines.inc $(SRCDIR)/gs2_init_level_list.inc

$(SRCDIR)/overrides.f90: $(SRCDIR)/generate_overrides.py
	$(call messageOp,"Generating from python","\\t:",$(call processName,$<))
	$(QUIETSYM)$(PYTHON_EXE) $< $@

ifeq ($(USE_NEW_DIAG),on)
$(SRCDIR)/diagnostics/diagnostics_ascii.f90: diagnostics/generate_diagnostics_ascii.py
	$(call messageOp,"Generating from python","\\t:",$(call processName,$<))
	$(QUIETSYM)$(PYTHON_EXE) $< $@
endif
endif

####################################################################### RULES

.SUFFIXES:

.PHONY: all $(GK_PROJECT)_all submodules
.PHONY: depend test_make help TAGS
.PHONY: clean cleanlib cleanconfig clean_coverage distclean
.PHONY: doc check_ford_install namelist_doc clean_namelist_doc cleandoc

####################################################################### Source code targets
$(BINDIR) $(LIBDIR) $(OBJDIR) $(MODDIR) $(GENERATED_SRC_DIR) $(AUTO_GEN_CONFIG_DIR):
	$(QUIETSYM)$(MKDIR) $@

%.o : %.pf
	$(PFUNIT_PREPROCESSOR) $< $(basename $<).F90
	$(QUIETSYM)$(FC) $(FPPFLAGS) $(F90FLAGS) -c $(basename $<).F90 $(PFUNIT_INC) -I$(GK_HEAD_DIR) -o $@

$(OBJDIR)/%.o: %.F90 |$(MODDIR) $(OBJDIR)
	$(call compile_and_preprocess)
ifeq ("$(COMPILER)","cray")
$(info When using cray you will need to run 'make clean' before rebuilding if you change any source files.)
$(info Alternatively you may simply remove the corresponding '$(GENERATED_SRC_DIR)/*.F90' file.)
#Special interim handling for cray which will only preprocess files
#with a limited set of suffixes (including F90). Here we just copy
#our source files to GENERATED_SRC_DIR with a F90 extension
# Keep the F90 files in GENERATED_SRC_DIR
.PRECIOUS: $(GENERATED_SRC_DIR)/%.F90
# Define rules for copying. Important that fpp is before f90
# so that we prefer the copy recipe rather than the preprocess one
$(GENERATED_SRC_DIR)/%.F90: %.fpp |$(GENERATED_SRC_DIR)
	$(QUIETSYM)cp $< $@
$(GENERATED_SRC_DIR)/%.F90: %.f90 |$(GENERATED_SRC_DIR)
	$(QUIETSYM)cp $< $@
# Rule to compile from GENERATED_SRC_DIR
$(OBJDIR)/%.o: $(GENERATED_SRC_DIR)/%.F90 |$(MODDIR) $(OBJDIR)
	$(call compile_and_preprocess)
else
$(OBJDIR)/%.o: %.fpp |$(MODDIR) $(OBJDIR)
	$(call compile_and_preprocess)
$(OBJDIR)/%.o: %.f90 |$(MODDIR) $(OBJDIR)
	$(call compile_and_preprocess)
endif

# The depend command needs the preprocessed source files.
# This is a little clunky, and maybe could be fixed in the future.
$(GENERATED_SRC_DIR)/%.f90: %.fpp |$(GENERATED_SRC_DIR) $(MODDIR)
	$(call preprocess)
$(GENERATED_SRC_DIR)/%.f90: %.F90 |$(GENERATED_SRC_DIR) $(MODDIR)
	$(call preprocess)

# This prevents error messages like m2c: Command not found
# This is from a implicit makefile rule that we're overriding
%.o : %.mod

# Special handling for creating a serial version of the mp module. Unfortunately
# if we replace FPPFLAGS_SERIAL with FPPFLAGS (e.g. to use compile_and_preprocess)
# then this impacts the compilation of any dependencies as well (e.g. in building mp.o).
# Note mp.o is a dependency to ensure that anything else which needs to be compiled
# at this point will still see `-DMPI`.
$(OBJDIR)/mp_serial.o: $(OBJDIR)/mp.o | $(MODDIR) $(OBJDIR)
$(OBJDIR)/mp_serial.o: FPPFLAGS_SERIAL:=$(subst -DMPI, ,$(subst -DMPI3, ,$(FPPFLAGS)))
$(OBJDIR)/mp_serial.o: $(UTILS)/mp.fpp | $(MODDIR) $(OBJDIR)
	$(call messageOp,"Compiling and preprocessing","\\t:",$(call processName,$<))
	$(QUIETSYM)$(FC) $(FPPFLAGS_SERIAL) $(F90FLAGS) $(MODDIR_FLAG)$(MODDIR) -c $< -o $@

DEPEND:=Makefile.depend
DEPEND_CMD:=$(PERL) scripts/fortdep

# Only define recipe to build dependency file if it doesn't exist
ifeq ("$(wildcard $(DEPEND))", "")
.PRECIOUS: $(DEPEND)
$(DEPEND): $(UTILS_SENTINEL)
$(DEPEND): $(UTILS_SENTINEL) depend
#Provide a no-op recipe
	@echo -n ""
endif

depend: $(UTILS_SENTINEL) $(NEASYF_SENTINEL) Makefile.$(GK_SYSTEM) |$(GENERATED_SRC_DIR)
	$(call messageOp,"Generating dependency file","\\t:",$(call processName,$(DEPEND)))
# Note we force UTILS to be included in the paths that we pass to fortdep
# We touch the dependency file first as fortdep will call make in order to try
# and preprocess files, so we need this to avoid recursion
	$(QUIETSYM)touch $(DEPEND) && $(DEPEND_CMD) -m "$(MAKE) --no-print-directory" -1 -o -v=0\
		$(VPATH) --generated_src_dir=$(GENERATED_SRC_DIR) || rm $(DEPEND)
	$(call messageOp,"Dependency file created at","\\t:",$(call processName,$(DEPEND)))

# Previously we dumpe dthe compilation flags to a file, so we can
# check if they change between invocations of `make`. The `cmp` bit
# checks if the file contents change. Adding a dependency of a file on
# `compiler_flags` causes it to be rebuilt when the flags
# change. Taken from https://stackoverflow.com/a/3237349/2043465
# Now we just store the git version and use that to trigger a rebuild of git_hash.o
GIT_VERSION_CONTENTS = "$(GIT_VERSION)"
.PHONY: force
git_version: force
	$(QUIETSYM)echo -e $(GIT_VERSION_CONTENTS) | cmp -s - $@ || echo -e $(GIT_VERSION_CONTENTS) > $@

# Things that should be rebuilt if the git version changes (for example, on
# a new commit). The object file should depend on git_version
$(OBJDIR)/git_hash.o: git_version

.PHONY: config_auto_gen
ifeq ($(GS2_HAS_PYTHON),yes)
config_auto_gen: |$(AUTO_GEN_CONFIG_DIR)
	$(call messageInfo,"Generating config object boilerplate.")
	$(QUIETSYM)$(GK_HEAD_DIR)/scripts/config_generation.py --include-only --target-dir $(AUTO_GEN_CONFIG_DIR)
else
config_auto_gen:
endif

##################################################################### Main TARGETS
# .DEFAULT_GOAL works for GNU make 3.81 (or higher)
# For 3.80 or less, see all target
.DEFAULT_GOAL := $(GK_PROJECT)_all
all: $(.DEFAULT_GOAL)

# Here we list all targets for which we don't care if there's a dependency file or not
# this can help us avoid the slow generation of the dependency file when not required
DONT_INCLUDE_DEPEND_TARGETS := clean distclean depend test_make submodules doc namelist_doc help config_auto_gen clean_auto_gen_config
# Note we really need to say do any of our targets not match the above as if we have multiple
# targets and any of them require the dependency file then we have to build the dependency file
# We achieve that using filter-out. Because we also want to be able to build when the user
# doesn't specify the target (i.e. just make -j ) it's not enough to check that MAKECMDGOALS is
# empty after filtering. We need to know if this was the case before hand.
ifneq ($(strip $(MAKECMDGOALS)),)
MUST_INCLUDE_DEPEND := $(filter-out $(DONT_INCLUDE_DEPEND_TARGETS),  $(MAKECMDGOALS))
else
MUST_INCLUDE_DEPEND := "YES"
endif

# If MUST_INCLUDE_DEPEND isn't empty then we better include the dependency file
ifneq ($(strip $(MUST_INCLUDE_DEPEND)),)
include $(DEPEND)
endif

############################################################# Get project specific targets
Makefile.target_$(GK_PROJECT): ;
include Makefile.target_$(GK_PROJECT)

############################################################# System config targets
ifdef STANDARD_SYSTEM_CONFIGURATION
system_config:
	@echo "#!/bin/bash " > system_config
	@echo "$(STANDARD_SYSTEM_CONFIGURATION)" >> system_config

else
.PHONY: system_config
system_config:
	$(error "STANDARD_SYSTEM_CONFIGURATION is not defined for this system")
endif

############################################################# Cleanup targets
clean: cleanobjs

cleanobjs:
	$(call messageRm,"Object and module files")
	-$(QUIETSYM)rm -rf $(OBJDIR)/*.o $(MODDIR)/*.mod

cleandirs:
	$(call messageRm,"Directories")
	-$(QUIETSYM)rm -rf $(MODDIR) $(OBJDIR) $(BINDIR) $(LIBDIR) $(GENERATED_SRC_DIR) $(COVERAGE_DIR) $(TEST_RESULT_DIR)

cleanlib:
	$(call messageRm,"Libraries")
	-$(QUIETSYM)rm -f $(LIBDIR)/*.a

cleanconfig:
	$(call messageRm,"System config")
	-$(QUIETSYM)rm -f system_config .tmp_output

clean_auto_gen_config:
	$(call messageRm,"Auto-generated config boilerplate")
	-$(QUIETSYM)rm -rf $(AUTO_GEN_CONFIG_DIR)

clean_depend:
	$(call messageRm,"Dependency file")	
	-$(QUIETSYM)rm -f $(DEPEND)

distclean: clean cleanlib cleanconfig cleandoc clean_depend cleandirs

############################################################# Testing targets
#This is the location of the individual test suites
$(TEST_DIR)/Makefile.tests_and_benchmarks: ;
sinclude $(TEST_DIR)/Makefile.tests_and_benchmarks

#This is the location of the pfunit test suites
PFUNIT_TEST_DIR:=$(TEST_DIR)/pfunit_tests
$(PFUNIT_TEST_DIR)/Makefile: ;
sinclude $(PFUNIT_TEST_DIR)/Makefile

############################################################# Introspection targets
test_make:
	$(call messageInfo,"Global settings:")
	$(call messageKeyVal, "GK_HEAD_DIR", $(GK_HEAD_DIR))
	$(call messageKeyVal, "GK_SYSTEM", $(GK_SYSTEM))
	$(call messageKeyVal, "VPATH", $(VPATH))
	$(call messageKeyVal, "CURDIR", $(CURDIR))
	$(call messageKeyVal, "MAKECMDGOALS", $(MAKECMDGOALS))
	$(call messageKeyVal, ".DEFAULT_GOAL", $(.DEFAULT_GOAL))
	$(call messageKeyVal, "DEFAULT_LIB", $(DEFAULT_LIB))
	$(call messageKeyVal, "DEPEND_CMD", $(DEPEND_CMD))
	$(call messageKeyVal, "DEPEND", $(DEPEND))
	$(call messageKeyVal, "QUIET", $(QUIET))
	$(call messageKeyVal, "WITH_COLOUR", $(WITH_COLOUR))
	$(call messageKeyVal, "MAKEFLAGS", $(MAKEFLAGS))
	$(call messageKeyVal, "GIT_VERSION", $(GIT_VERSION))
	$(call messageKeyVal, "GIT_HASH", $(GIT_HASH))
	$(call messageKeyVal, "GIT_HASH_MAKEFILES", $(GIT_HASH_MAKEFILES))
	$(call messageKeyVal, "GIT_HASH_UTILS", $(GIT_HASH_UTILS))
	$(call messageKeyVal, "GIT_STATE", $(GIT_STATE))
	$(call messageKeyVal, "GIT_BRANCH", $(GIT_BRANCH))
	$(call messageKeyVal, "PATCHFILE", $(PATHFILE))
	$(call messageKeyVal, "IS_RELEASE", $(IS_RELEASE))
	$(call messageKeyVal, "RELEASE", $(RELEASE))
	@echo
	$(call messageInfo,"Commands:")
	$(call messageKeyVal, "COMPILER", $(COMPILER))
	$(call messageKeyVal, "FC", $(FC))
	$(call messageKeyVal, "LD", $(LD))
	$(call messageKeyVal, "CPP", $(CPP))
	$(call messageKeyVal, "ARCH", $(ARCH))
	$(call messageKeyVal, "RANLIB", $(RANLIB))
	$(call messageKeyVal, "PERL", $(PERL))
	$(call messageKeyVal, "SHELL", $(SHELL))
	$(call messageKeyVal, "MAKE", $(MAKE))
	$(call messageKeyVal, "FORD", $(FORD))
	$(call messageKeyVal, "MKDIR", $(MKDIR))
	@echo
	$(call messageInfo,"Compilation mode:")
	$(call messageKeyVal, "DEBUG", $(DEBUG))
	$(call messageKeyVal, "SCAL", $(SCAL))
	$(call messageKeyVal, "PROF", $(PROF))
	$(call messageKeyVal, "OPT", $(OPT))
	$(call messageKeyVal, "STATIC", $(STATIC))
	$(call messageKeyVal, "DBLE", $(DBLE))
	@echo
	$(call messageInfo,"Functionality:")
	$(call messageKeyVal, "USE_MPI", $(USE_MPI))
	$(call messageKeyVal, "USE_SHMEM", $(USE_SHMEM))
	$(call messageKeyVal, "USE_OPENMP", $(USE_OPENMP))
	$(call messageKeyVal, "USE_FFT", $(USE_FFT))
	$(call messageKeyVal, "USE_NETCDF", $(USE_NETCDF))
	$(call messageKeyVal, "USE_PARALLEL_NETCDF", $(USE_PARALLEL_NETCDF))
	$(call messageKeyVal, "USE_NETCDF_COMPRESSION", $(USE_NETCDF_COMPRESSION))
	$(call messageKeyVal, "USE_EIG", $(USE_EIG))
	$(call messageKeyVal, "USE_LAPACK", $(USE_LAPACK))
	$(call messageKeyVal, "USE_LOCAL_RAN", $(USE_LOCAL_RN))
	$(call messageKeyVal, "USE_LOCAL_SPFUNC", $(USE_LOCAL_SPFUNC))
	$(call messageKeyVal, "USE_NEW_DIAG", $(USE_NEW_DIAG))
	$(call messageKeyVal, "USE_GCOV", $(USE_GCOV))
	@echo
	$(call messageInfo,"Flags:")
	$(call messageKeyVal, "F90FLAGS", $(F90FLAGS))
	$(call messageKeyVal, "F90OPTFLAGS", $(F90OPTFLAGS))
	$(call messageKeyVal, "LDFLAGS", $(LDFLAGS))
	$(call messageKeyVal, "FPPFLAGS", $(FPPFLAGS))
	$(call messageKeyVal, "ARCHFLAGS", $(ARCHFLAGS))
	$(call messageKeyVal, "MACRO_DEFS", $(MACRO_DEFS))
	$(call messageKeyVal, "LD_LIBRARY_PATH", $(LD_LIBRARY_PATH))
	$(call messageKeyVal, "LIBS", $(LIBS))
	$(call messageKeyVal, "INCLUDES", $(INCLUDES))
	@echo
	$(call messageInfo,"Build info:")
	$(call messageKeyVal, "SRCDIR ", $(SRCDIR))
	$(call messageKeyVal, "PROGDIR", $(PROGDIR))
	$(call messageKeyVal, "BINDIR ", $(BINDIR))
	$(call messageKeyVal, "LIBDIR ", $(LIBDIR))
	$(call messageKeyVal, "OBJDIR ", $(OBJDIR))
	$(call messageKeyVal, "MODDIR ", $(MODDIR))
	$(call messageKeyVal, "SUBDIRS", $(SUBDIRS))
	$(call messageKeyVal, "GENERATED_SRC_DIR", $(GENERATED_SRC_DIR))
	$(call messageKeyVal, "COVERAGE_DIR", $(COVERAGE_DIR))
	$(call messageKeyVal, "TEST_RESULT_DIR", $(TEST_RESULT_DIR))
	@echo
	$(call messageInfo,"PYTHON:")
	$(call messageKeyVal, "GS2_HAS_PYTHON", $(GS2_HAS_PYTHON))
	$(call messageKeyVal, "GS2_PYTHON_NAME", $(GS2_PYTHON_NAME))
	$(call messageKeyVal, "PYTHON_EXE", $(PYTHON_EXE))
	$(call messageKeyVal, "PYTHON_VERSION", $(PYTHON_VERSION))
	@echo
	$(call messageInfo,"PFUNIT:")
	$(call messageKeyVal, "PFUNIT_VERSION_MAJOR", $(PFUNIT_VERSION_MAJOR))
	$(call messageKeyVal, "PFUNIT_PREPROCESSOR", $(PFUNIT_PREPROCESSOR))
	$(call messageKeyVal, "PFUNIT_TESTS", $(PFUNIT_TESTS))
	$(call messageKeyVal, "PFUNIT_NPROC", $(PFUNIT_NPROC))
	$(call messageKeyVal, "PFUNIT_MOD", $(PFUNIT_MOD))
	$(call messageKeyVal, "PFUNIT_TEST_DIR", $(PFUNIT_TEST_DIR))
	$(call messageKeyVal, "PFUNIT_TEST_EXE", $(PFUNIT_TEST_EXE))
	$(call messageKeyVal, "PFUNIT_DRIVER", $(PFUNIT_DRIVER))
	$(call messageKeyVal, "PFUNIT_INC", $(PFUNIT_INC))
	$(call messageKeyVal, "PFUNIT_LIB", $(PFUNIT_LIB))
	@echo
ifdef USE_MPI
	$(call messageInfo,"MPI:")
	$(call messageKeyVal, "MPI_MOD_NAME", $(MPI_MOD_NAME))
	$(call messageKeyVal, "MPI_LIB", $(MPI_LIB))
	$(call messageKeyVal, "MPI_INC", $(MPI_INC))
else
	$(call messageInfo,"MPI: DISABLED")
endif
	@echo
ifdef USE_NETCDF
	$(call messageInfo,"Netcdf:")
	$(call messageKeyVal, "NETCDF_LIB", $(NETCDF_LIB))
	$(call messageKeyVal, "NETCDF_INC", $(NETCDF_INC))
else
	$(call messageInfo,"Netcdf: DISABLED")
endif
	@echo
ifdef USE_FFT
	$(call messageInfo,"FFT:")
	$(call messageKeyVal, "FFT_LIB", $(FFT_LIB))
	$(call messageKeyVal, "FFT_INC", $(FFT_INC))
	$(call messageKeyVal, "FFT_DIR", $(FFT_DIR))
else
	$(call messageInfo,"FFT: DISABLED")
endif
	@echo
ifdef USE_EIG
	$(call messageInfo,"Eigensolver:")
	$(call messageKeyVal, "EIG_LIB", $(EIG_LIB))
	$(call messageKeyVal, "EIG_INC", $(EIG_INC))
else
	$(call messageInfo,"Eigensolver: DISABLED")
endif
	@echo
ifdef USE_LAPACK
	$(call messageInfo,"Lapack:")
	$(call messageKeyVal, "LAPACK_LIB", $(LAPACK_LIB))
	$(call messageKeyVal, "LAPACK_INC", $(LAPACK_INC))
else
	$(call messageInfo,"Lapack: DISABLED")
endif
	@echo

revision:
	$(QUIETSYM)LANG=C git rev-parse HEAD > Revision

#This only works if a helplocal rule has been defined in the specific system makefile.
#It would be nice to provide a default helplocal target that just says no help available
help: helplocal

TAGS: $(ALL_SOURCE_FILES)
	$(QUIETSYM)etags $^

############################################################# Documentation

ifneq ("$(wildcard $(shell which $(FORD) 2>/dev/null))","")
check_ford_install:
	$(call messageInfo,"Using ford at $(shell which $(FORD))")
else
check_ford_install:
	$(call messageInfo,"Ford command $(FORD) not in path -- is it installed?\\n\\tConsider installing with 'pip install --user ford' and add ${HOME}/.local/bin to PATH") ; which $(FORD)
endif

namelist_doc: PATHS:=$(foreach dir, $(SRCDIR) $(SUBDIRS), --path ${dir})
namelist_doc: FILES:=$(shell $(GK_HEAD_DIR)/scripts/get_all_matching_files.py --pattern "*.[fF]??" ${PATHS} --absolute)
# for the `doc` target, `check_ford_install` must be before `namelist_doc` to ensure visibility of the error message
# in the case where Ford is not installed and `make` is run with the `-j` option. The most robust way to do this is
# to have `namelist_doc` to depend explicitly on `check_ford_install` as below
namelist_doc: check_ford_install
	$(call messageInfo,"Generating namelist documentation")
	$(QUIETSYM)$(GK_HEAD_DIR)/scripts/generate_namelist_documentation.py ${FILES}

clean_namelist_doc:
	$(call messageRm,"Generated namelist documentation")
	-$(QUIETSYM)rm -f docs/page/namelists/auto_generated_* docs/page/namelists/index.md

doc: docs/docs.config.md namelist_doc  check_ford_install
	$(FORD) $(subst -D,--macro ,$(MACRO_DEFS)) $(INCLUDES) -r $(GIT_HASH) $<

cleandoc: clean_namelist_doc
	$(call messageRm,"FORD docs")	
	-$(QUIETSYM)rm -rf docs/html docs/graphs
